﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Угадай_число
{
    class Program
    {
        static void Main(string[] args)
        {
            int usernumb = 0, i = 0, top = 100, bot = 1, level = 0, number, hardmode = 2;
            double final, death = 10;
            
            #region Сложность
            Console.WriteLine(
@"Выберите уровень сложности
1) Легкий   Число от 1 до 10
2) Средний  Число от 1 до 100
3) Сложный  Число от 1 до 1000
");
            if (int.TryParse(Console.ReadLine(), out level))
            {
                switch (level)
                {
                    case 1:
                        top = 10;
                        break;
                    case 3:
                        top = 1000;
                        break;
                    default:
                        top = 100;
                        break;
                }
            }
            else
            {
                Console.WriteLine("Неверный формат ввода, установлена средняя сложность.");
            }
            
            final = top;
            death = top * 0.1;
            #endregion
            #region Hardmode
            if (top >= 100)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(@"
Включить хардмод?
(1) - да      (2) - нет"
                );
                Console.ResetColor();
                if (int.TryParse(Console.ReadLine(), out hardmode))
                {
                    switch (hardmode)
                    {
                        case 1:
                            hardmode = 1;
                            break;
                        case 2:
                            hardmode = 2;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Вы ввели неверные данные, хардмод выключен.");
                            Console.ResetColor();
                            hardmode = 2;
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Неверный формат ввода, хардмод выключен.");
                }
                
                
            }
            #endregion
            #region Тело
            Random rnd = new Random();
            number = rnd.Next(bot,top);
            do
            {
                i++;
                Console.WriteLine();
                if (hardmode == 1)
                {
                    Console.Write
(
$@"Попытка {i}.
Осталось попыток: {death}.

Число от {bot} до {top}
Отгадай число: "
);
                    death--;
                }
                else
                {
                    Console.Write
(
$@"Попытка {i}
Число от {bot} до {top}
Отгадай число: "
);
                }

                if (int.TryParse(Console.ReadLine(), out usernumb))
                {}
                else
                {
                    Console.WriteLine("Неверный формат числа, число не изменится. для продолжения нажмите любую клавишу...");
                    Console.ReadLine();
                }
                    ;

                if (death == 0)
                {
                    Console.Clear();
                    Console.WriteLine("Попытки кончились, ты проиграл.");
                }
                else if (usernumb == number)

                {
                    Console.Clear();    
                    Console.WriteLine
(
$@"Вы отгадали число! Поздравляю!
Затрачено попыток: {i}."
);
                }
                else if (usernumb > number)
                {
                    Console.Clear();
                    Console.WriteLine("Введи число поменьше");
                    if (top > usernumb)
                    {
                        top = usernumb;
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Введи число побольше");
                    if (bot < usernumb)
                    {
                        bot = usernumb;
                    }
                }
            }
            while (usernumb != number && death != 0);
            #endregion
            #region Оценка
            if (i <= final * 0.05)
            {
                Console.WriteLine("Ты бог рандома");
            }
            else if (i > final * 0.05 && i < final * 0.1)
            {
                Console.WriteLine("Отлично справился!");
            }
            else if (i >= final * 0.1 && i <= final * 0.2)
            {
                Console.WriteLine("Можно было бы и побыстрее угадать...");
            }
            else
            {
                Console.WriteLine("Никуда не годится.");
            }
            #endregion
            Console.WriteLine();
            Console.WriteLine("Для выхода нажми любую клавишу...");
            Console.ReadKey();
        }
    }
}
